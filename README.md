ARCHIVED
========

This project has been archived and will not be updated anymore, it has been integrated in the Miraverse project. It only remain here for historic purpose.


librarium.so
============

About
-----

Core library for SFML application development

Build
-----

Should be built along side an application

License
-------

All Drawing source code and assets are provided under the MIT license (See LICENSE file)
