// Copyright RariumUnlimited - Licence : MIT
#include "Rarium/Tools/Util.h"

namespace RR {

std::vector<std::string> Split(const std::string& stringToSplit,
                               char separator,
                               char ignore) {
    // Where we store the list of strings
    std::vector<std::string> strings;

    // Store the value of the current split string
    std::string currentString;

    for (unsigned int i = 0; i < stringToSplit.size(); ++i) {
        // If we encounter a space while we are not in an argument it means it's the end of the current argument
        if (stringToSplit[i] == separator) {
            if (currentString != "") {
                strings.push_back(currentString);
                currentString = "";
            }
        } else if (stringToSplit[i] == ignore && ignore != std::numeric_limits<char>::min()) {
            continue;
        } else {
            currentString += stringToSplit[i];
        }
    }

    // If we have something in the value of the current, we must add it to the argument list.
    if (currentString != "") {
        strings.push_back(currentString);
        currentString = "";
    }

    return strings;
}

}  // namespace RR
