// Copyright RariumUnlimited - Licence : MIT
#include "Rarium/Tools/FileLoader.h"

#include <fstream>

namespace RR {

std::string LoadFileAsString(std::string path, std::string baseDirectory) {
    std::ifstream ifs;

    ifs.open((baseDirectory + path).c_str(), std::ifstream::in);

    if (!ifs.good())
        throw std::string("Could not read file : " + baseDirectory + path);

    std::string result;

    while (!ifs.eof()) {
        std::string line;
        std::getline(ifs, line);
        result += line + "\n";
    }

    return result;
}

std::vector<char> LoadFileAsVector(std::string path, std::string baseDirectory) {
    std::ifstream ifs;

    ifs.open((baseDirectory + path).c_str(), std::ifstream::in);

    if (!ifs.good())
        throw std::string("Could not read file : " + baseDirectory + path);

    ifs.seekg(0, ifs.end);
    std::streamoff length = ifs.tellg();
    ifs.seekg(0, ifs.beg);

    std::vector<char> result;

    result.resize(length);

    ifs.read(result.data(), length);

    ifs.close();

    return result;
}
}  // namespace RR
