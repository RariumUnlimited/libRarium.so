// Copyright RariumUnlimited - Licence : MIT
#include "Rarium/Tools/Log.h"

#include <chrono>
#include <iostream>
#include <string>

namespace RR {

void Logger::AddStream(std::ostream* stream) {
    streams.push_back(stream);
}

Logger& Logger::Log(bool outputTime) {
    static Logger instance;

    if (outputTime) {
        std::chrono::system_clock::time_point today = std::chrono::system_clock::now();
        std::time_t tt = std::chrono::system_clock::to_time_t(today);

        std::string timeStr = ctime(&tt);
        timeStr.pop_back();

        instance << timeStr << ": ";
    }

    return instance;
}

}  // namespace RR
