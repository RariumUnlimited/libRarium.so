// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RARIUM_TOOLS_FILELOADER_H_

#define SRC_RARIUM_TOOLS_FILELOADER_H_

#include <string>
#include <vector>

#include <SFML/System.hpp>

namespace RR {

/**
 *    Load a file as a string
 *    @param path Path to the file in the assets directory
 *    @param baseDirectory Directory to prepend to the path argument
 *    @return A string which represent the content of a file
 */
std::string LoadFileAsString(std::string path, std::string baseDirectory = "./Assets/");
/**
 *    Load a file as a vector of char
 *    @param path Path to the file in the assets directory
 *    @param baseDirectory Directory to prepend to the path argument
 *    @return A vector which contains the files
 */
std::vector<char> LoadFileAsVector(std::string path, std::string baseDirectory = "./Assets/");

}  // namespace RR

#endif  // SRC_RARIUM_TOOLS_FILELOADER_H_
