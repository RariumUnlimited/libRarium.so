// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RARIUM_TOOLS_LOG_H_

#define SRC_RARIUM_TOOLS_LOG_H_

#include <ostream>
#include <string>
#include <vector>

#include <SFML/System.hpp>

namespace RR {

/**
 *    Singleton used to log message to a file, this class is thread safe
 */
class Logger : public sf::NonCopyable {
 public:
    /**
     * @brief Get a reference to the logger
     * @param outputTime If the method must output to in the logger before returning
     * @return A reference to the logger
     */
    static Logger& Log(bool outputTime = true);

    /**
     * @brief Add to stream to ouput logs
     * @param stream Reference to a stream where we want to output logs
     */
    void AddStream(std::ostream* stream);

    template<typename T>
    friend Logger& operator<<(Logger& log, T t);

 private:
    /// Build a logger
    Logger() { }

    std::vector<std::ostream*> streams;
};

template<typename T>
Logger& operator<<(Logger& log, T t) {
    for (std::ostream* s : log.streams) {
        *s << t;
    }
    return log;
}

}  // namespace RR

#endif  // SRC_RARIUM_TOOLS_LOG_H_
