// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RARIUM_TOOLS_UTIL_H_

#define SRC_RARIUM_TOOLS_UTIL_H_

#include <limits>
#include <string>
#include <sstream>
#include <vector>

namespace RR {

/**
 *    Convert an object to a string.
 */
template <typename T>
inline std::string ToString(const T& t) {
    std::ostringstream os;
    os << t;
    return os.str();
}


/**
 *    Convert a string to an object.
 */
template <typename T>
inline T ToObject(const std::string& str) {
    T result;
    std::istringstream is(str);
    is >> result;
    return result;
}

/**
 *    Split a string in a vector of string depending on a specific character
 *    @param stringToSplit The string that we want to split
 *    @param seperator The char used to know when we need to split
 *    @param ignore Ignore a specific character (only if ignore != std::numeric_limits<char>::min())
 *    @return A vector containing all the substring of stringToSplit
 */
std::vector<std::string> Split(const std::string& stringToSplit,
                               char separator,
                               char ignore = std::numeric_limits<char>::min());

}  // namespace RR

#endif  // SRC_RARIUM_TOOLS_UTIL_H_
