// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RARIUM_TOOLS_MATH_H_

#define SRC_RARIUM_TOOLS_MATH_H_

#include <cassert>

#include <SFML/Graphics.hpp>

namespace RR {

/**
 * Template function to interpolate two values depending on a third
 * @param a First value
 * @param b Second value
 * @param t Interpolation amount, 0.f <= t <= 1.f
 */
template <typename T>
T Lerp(T a, T b, float t) {
    assert(t >= 0.f && t <= 1.f);
    return (1 - t) * a + t * b;
}

/**
 *	Template function to clamp a value between a min and a max
 *	@param value Value to clamp
 *	@param min Minimum value
 *	@param max Maximum value
 *	@return min if value < min, max if value > max or value
 */
template <typename T>
inline T Clamp(T value, T min, T max) {
    return value <= max ? (value >= min ? value : min) : max;
}

}  // namespace RR

#endif  // SRC_RARIUM_TOOLS_MATH_H_
