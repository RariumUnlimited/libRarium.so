// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RARIUM_TOOLS_CATALOG_H_

#define SRC_RARIUM_TOOLS_CATALOG_H_

#include <map>
#include <memory>
#include <string>
#include <utility>

#include <SFML/System.hpp>

namespace RR {

/// A catalog of unique assets
template<typename A>
class Catalog : public sf::NonCopyable {
 public:
    Catalog() { }
    /**
     *	Add a new asset to the catalog.
     *	@param name Name of the asset
     *	@param args Arguments for the new object constructor
     *	@return Pointer to the asset
     */
    template<typename... Args>
    A* Add(std::string name, Args... args) {
        auto[it, result] = assets.try_emplace(name, nullptr);

        if (result)
            it->second = std::make_unique<A>(std::forward<Args>(args)...);

        return it->second.get();
    }

    /**
     *	Add a new asset to the catalog while defining a child class of A.
     *	@param name Name of the asset
     *	@param args Arguments for the new object constructor
     *	@return Pointer to the asset
     */
    template<typename C, typename... Args>
    A* Add(std::string name, Args... args) {
        auto[it, result] = assets.try_emplace(name, nullptr);

        if (result)
            it->second = std::make_unique<C>(std::forward<Args>(args)...);

        return it->second.get();
    }

    /**
     *	Override an existing asset to the catalog. The given pointer is now the property of this catalog, no need to delete it.
     *	@param name Name of the asset
     *	@param args Arguments for the new object constructor
     *	@return Pointer to the asset
     */
    template<typename... Args>
    A* Override(std::string name, Args... args) {
        A* result;
        std::unique_ptr<A> temp = std::make_unique<A>(std::forward<Args>(args)...);
        result = temp.get();
        assets[name] = std::move(temp);

        return result;
    }

    /**
     *	Override an existing asset to the catalog using a child class. The given pointer is now the property of this catalog, no need to delete it.
     *	@param name Name of the asset
     *	@param args Arguments for the new object constructor
     *	@return Pointer to the asset
     */
    template<typename C, typename... Args>
    A* Override(std::string name, Args... args) {
        A* result;
        std::unique_ptr<A> temp = std::make_unique<C>(std::forward<Args>(args)...);
        result = temp.get();
        assets[name] = std::move(temp);

        return result;
    }

    /**
    *	Delete an asset in the catalog
    *	@param name Name of the asset to delete
    */
    void Delete(std::string name) {
        assets.erase(name);
    }

    /// Access an asset using the [] operator
    A* operator[](std::string name) { return Get(name); }

    /// Purge the catalog of its assets
    inline void Purge() {
        assets.clear();
    }
    /// Get an asset, or nullptr if it doesn't exist
    inline A* Get(std::string name) {
        auto it = assets.find(name);
        return it != assets.end() ? it->second.get() : nullptr;
    }

 private:
    std::map<std::string, std::unique_ptr<A>> assets;  ///< All assets currently loaded
};

}  // namespace RR

#endif  // SRC_RARIUM_TOOLS_CATALOG_H_
