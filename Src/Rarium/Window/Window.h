// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RARIUM_WINDOW_WINDOW_H_

#define SRC_RARIUM_WINDOW_WINDOW_H_

#include <string>
#include <unordered_map>

#include <SFML/Graphics.hpp>

#include "Rarium/Tools/Event.h"
#include "Rarium/Window/InputState.h"

namespace RR {

typedef Event<void, const InputState&, sf::Mouse::Button> MouseButtonEvent;
typedef Event<void, const InputState&, sf::Mouse::Button, sf::Vector2i> MouseDragEvent;
typedef Event<void, const InputState&, float> MouseWheelEvent;
typedef Event<void, const InputState&, sf::Keyboard::Key> KeyEvent;

class Window {
 public:
    /**
     *    Initialize the sfml window
     *    @param title Title of the window
     *    @param mode Size of the window to create
     *    @param style Style of the window (Fullscreen, ...)
     *    @param setting OpenGL context to use
     */
    Window(std::string title,
           sf::VideoMode mode = sf::VideoMode(1280, 720),
           sf::Uint32 style = sf::Style::Close,
           sf::ContextSettings settings = sf::ContextSettings());

    virtual ~Window() { }

    /**
     *    Launch the app
     */
    void Run();

    /**
     * @brief Draw
     * @param delta Time in seconds since last call to Draw
     */
    virtual void Draw(float delta, sf::RenderTarget& target) = 0;

    /**
     *    Get the state of user input state of the current update
     *    @return Pointer to the input state of the current update
     */
    inline const InputState& GetCurrentInput() const { return currentInput; }
    /**
     *    Get the state of user input state of the last update
     *    @return Pointer to the input state of the last update
     */
    inline const InputState& GetLastUpdateInput() const { return lastInput; }

    MouseButtonEvent MouseButtonPressedEvent;  ///< Event when the user press a mouse button
    MouseButtonEvent MouseButtonReleasedEvent;  ///< Event when the user release a mouse button
    MouseButtonEvent MouseButtonClickEevent;  ///< Event when the use click
    MouseButtonEvent MouseButtonDoubleClickEvent;  ///< Event when the user double click
    MouseButtonEvent MouseBeginDrag;  ///< Event when the user start dragging
    MouseButtonEvent MouseEndDrag;  ///< Event when the user start dragging
    MouseDragEvent MouseDrag;  ///< Event when the user drag the mouse
    MouseWheelEvent MouseWheelScrolledEvent;  ///< Event when the user uses his mouse wheel
    KeyEvent KeyPressedEvents;  ///< For each key we have a corresponding event (when the user press a key)
    KeyEvent KeyReleasedEvents;  ///< For each key we have a corresponding event (when the user releases a key)
    Event<void> CloseEvent;  ///< Event when the user closes the window

 protected:
    /**
     * @brief Close the window
     */
    inline void Close() { window.close(); }

    /**
     * @brief Make a custom action when receiving a sf::Event
     */
    virtual void CustomEventProcess(sf::Event e) { }

    sf::RenderWindow window;  ///< Our sfml window

 private:
    void HandleEvents();
    void HandleMouseButtonDown(sf::Event e);
    void HandleMouseButtonUp(sf::Event e);
    void HandleMouseWheel(sf::Event e);
    void HandleKeyPressed(sf::Event e);
    void HandleKeyReleased(sf::Event e);

    /**
     * @brief Take of a screenshot of the window content
     */
    void TakeScreenshot();

    InputState currentInput;  ///< User input state of the current update
    InputState lastInput;  ///< User input state of the last update

    sf::Clock doubleClickClock[sf::Mouse::Button::ButtonCount];  ///< Clock used to check if we double click
    sf::Clock clickClock[sf::Mouse::Button::ButtonCount];  ///< Clock used to check clicking
    bool dragging[sf::Mouse::Button::ButtonCount];  ///< If we are dragging
};

}  // namespace RR

#endif  // SRC_RARIUM_WINDOW_WINDOW_H_
