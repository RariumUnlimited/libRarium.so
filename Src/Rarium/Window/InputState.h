// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RARIUM_WINDOW_INPUTSTATE_H_

#define SRC_RARIUM_WINDOW_INPUTSTATE_H_

#include <unordered_set>
#include <string>

#include <SFML/Window.hpp>

namespace RR {

/**
 * State of user input
 */
struct InputState {
    sf::Vector2i MousePosition = sf::Vector2i(0, 0);  ///< Mouse position on screen
    float WheelAmount = 0.f;  ///< Middle scroll wheel amount
    std::unordered_set<sf::Keyboard::Key> KeyPressed;  ///< The list of key that are being pressed
    std::unordered_set<sf::Mouse::Button> ButtonPressed;  ///< The list of key that are being pressed
};

}  // namespace RR

#endif  // SRC_RARIUM_WINDOW_INPUTSTATE_H_
