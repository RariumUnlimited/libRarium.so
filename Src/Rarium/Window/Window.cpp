// Copyright RariumUnlimited - Licence : MIT
#include "Rarium/Window/Window.h"

#include <chrono>

#include "Rarium/Tools/Log.h"

namespace RR {

Window::Window(std::string title,
       sf::VideoMode mode,
       sf::Uint32 style,
       sf::ContextSettings settings) :
    window(mode, title, style, settings) {
    if (window.isOpen())
        Logger::Log() << "Window opened, Width: " << mode.width <<
                         "px, Height: " << mode.height << "px\n";
    else
        throw std::string("Unable to open SFML window.");

    CloseEvent += new Event<void>::T<sf::Window>(&window, &sf::Window::close);

    for (int b = 0; b < sf::Mouse::ButtonCount; ++b)
        dragging[b] = false;
}

void Window::Run() {
    // Reset the clock for delta time calculation
    sf::Clock clock;
    while (window.isOpen()) {
        HandleEvents();

        window.clear(sf::Color::Black);

        float delta = clock.restart().asSeconds();

        Draw(delta, window);

        window.display();

        lastInput = currentInput;
    }
}

void Window::HandleEvents() {
    lastInput = currentInput;
    currentInput.WheelAmount = 0;

    sf::Event e;
    while (window.pollEvent(e)) {
        CustomEventProcess(e);
        if (e.type == sf::Event::Closed)
            CloseEvent();
        else if (e.type == sf::Event::MouseButtonPressed)
            HandleMouseButtonDown(e);
        else if (e.type == sf::Event::MouseButtonReleased)
            HandleMouseButtonUp(e);
        else if (e.type == sf::Event::MouseWheelScrolled)
            HandleMouseWheel(e);
        else if (e.type == sf::Event::KeyPressed)
            HandleKeyPressed(e);
        else if (e.type == sf::Event::KeyReleased)
            HandleKeyReleased(e);
    }

    currentInput.MousePosition = sf::Mouse::getPosition(window);

    for (int b = 0; b < sf::Mouse::ButtonCount; ++b) {
        if (currentInput.ButtonPressed.find(sf::Mouse::Button(b)) == currentInput.ButtonPressed.cend()) {
            if (dragging[b]) {
                MouseEndDrag(currentInput, sf::Mouse::Button(b));
                dragging[b] = false;
            }
        } else {
            if (!dragging[b] && clickClock[b].getElapsedTime().asMilliseconds() > 200) {
                MouseBeginDrag(currentInput, sf::Mouse::Button(b));
                dragging[b] = true;
            } else if (dragging[b]) {
                sf::Vector2i offset = currentInput.MousePosition - lastInput.MousePosition;
                if (offset.x != 0 || offset.y != 0)
                    MouseDrag(currentInput, sf::Mouse::Button(b), offset);
            }
        }
    }
}

void Window::HandleMouseButtonDown(sf::Event e) {
    currentInput.ButtonPressed.insert(e.mouseButton.button);

    clickClock[e.mouseButton.button].restart();

    MouseButtonPressedEvent(currentInput, e.mouseButton.button);

}

void Window::HandleMouseButtonUp(sf::Event e) {
    currentInput.ButtonPressed.erase(e.mouseButton.button);

    if (clickClock[e.mouseButton.button].getElapsedTime().asMilliseconds() < 200) {
        if (doubleClickClock[e.mouseButton.button].getElapsedTime().asMilliseconds() < 200) {
            MouseButtonDoubleClickEvent(currentInput, e.mouseButton.button);
        } else {
            MouseButtonClickEevent(currentInput, e.mouseButton.button);
            doubleClickClock[e.mouseButton.button].restart();
        }
    }

    MouseButtonReleasedEvent(currentInput, e.mouseButton.button);
}

void Window::HandleMouseWheel(sf::Event e) {
    currentInput.WheelAmount = e.mouseWheelScroll.delta;
}

void Window::HandleKeyPressed(sf::Event e) {
    currentInput.KeyPressed.insert(e.key.code);

    if (e.key.code == sf::Keyboard::Key::F12)
        TakeScreenshot();

    KeyPressedEvents(currentInput, e.key.code);
}

void Window::HandleKeyReleased(sf::Event e) {
    currentInput.KeyPressed.erase(e.key.code);

    KeyReleasedEvents(currentInput, e.key.code);
}

void Window::TakeScreenshot() {
    sf::Texture screenTex;
    screenTex.create(window.getSize().x, window.getSize().y);
    screenTex.update(window);
    sf::Image screenImg = screenTex.copyToImage();
    std::time_t time =
            std::chrono::system_clock::to_time_t(
                std::chrono::system_clock::now());
    char datetime[512];
    std::size_t datetimesize =
            strftime(datetime, 512, "%Y%m%d-%H%M%S", std::localtime(&time));
    std::string filename(datetime, datetimesize);
    filename = "screenshot-"+filename+".png";
    screenImg.saveToFile(filename);
}

}  // namespace RR
