// Copyright RariumUnlimited - Licence : MIT
#include "Rarium/Ui/Ui.h"

namespace RR {

void Ui::Update(float delta,
                const InputState& state,
                const InputState& lastState) {
    UiElement::ResetMouseInterception();
    for (int i = static_cast<int>(elements.size()) - 1; i >= 0; i--)
        elements[i]->Update(delta, state, lastState);
}

UiElement* Ui::AddElement(UiElement* elem) {
    elements.push_back(elem);
    return elem;
}

void Ui::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    target.pushGLStates();
    for (UiElement* e : elements)
        target.draw(*e);
    target.popGLStates();
}

}  // namespace RR
