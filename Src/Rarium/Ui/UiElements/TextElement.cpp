// Copyright RariumUnlimited - Licence : MIT
#include "Rarium/Ui/UiElements/TextElement.h"

#include <string>

namespace RR {

TextElement::TextElement(Ui* ui,
                         std::string id,
                         sf::Vector2f position,
                         std::string text,
                         TextCharacteristic charac,
                         UiElement* parent) :
    UiElement(ui, id, position, parent) {
    font.loadFromFile(charac.Font);

    this->text.setFont(font);
    this->text.setFillColor(charac.Color);
    this->text.setString(text);
    this->text.setCharacterSize(charac.FontSize);
    this->text.setPosition(this->GetPosition().x, this->GetPosition().y);

    drawables.push_back(&this->text);
}

void TextElement::UpdateText(std::string text) {
    this->text.setString(text);
}

void TextElement::ElementMoved(sf::Vector2f offset) {
    sf::Vector2f pos = text.getPosition();
    text.setPosition(pos.x + offset.x, pos.y + offset.y);
}

}  // namespace RR
