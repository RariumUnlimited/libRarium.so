// Copyright RariumUnlimited - Licence : MIT
#include "Rarium/Ui/UiElements/ButtonElement.h"

namespace RR {

ButtonElement::ButtonElement(Ui* ui,
                             std::string id,
                             sf::Vector2f position,
                             ButtonSprites sprites,
                             std::string text,
                             TextCharacteristic charac,
                             UiElement* parent) :
    ImageElement(ui, sprites.Def, position, parent),
    textElem(ui, id+"Text", sf::Vector2f(), text, charac, this) {
    textureActive.loadFromFile(sprites.Active);
    textureHover.loadFromFile(sprites.Hover);

    children.push_back(&textElem);
    sf::FloatRect bounds = textElem.GetSFText().getLocalBounds();
    textElem.MoveFromOffset(
                sf::Vector2f((texture.getSize().x / 2) - (bounds.width / 2),
                             (charac.FontSize / 2) - (bounds.height / 2)));
    children.back()->SetEnabled(false);
}

void ButtonElement::Update(float delta,
                           const InputState& inputState,
                           const InputState& lastInputState) {
    UiElement::Update(delta, inputState, lastInputState);

    const UiElementState& state = GetState();

    if (state.IsActive)
        sprite.setTexture(textureActive);
    else if (state.IsHover)
        sprite.setTexture(textureHover);
    else
        sprite.setTexture(texture);
}

}  // namespace RR
