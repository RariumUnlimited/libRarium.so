// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RARIUM_UI_UIELEMENTS_BUTTONELEMENT_H_

#define SRC_RARIUM_UI_UIELEMENTS_BUTTONELEMENT_H_

#include <memory>
#include <string>

#include "Rarium/Ui/UiElements/ImageElement.h"
#include "Rarium/Ui/UiElements/TextElement.h"

namespace RR {

/// Sprites for the different state of a button
struct ButtonSprites {
    std::string Def;  ///< Default sprite
    std::string Active;  ///< Active sprite
    std::string Hover;  ///< Hover sprite
};

/**
 *    A button that can be clicked
 */
class ButtonElement : public ImageElement {
 public:
    /**
     * Create a new button
     *    @param ui The ui possessing this element
     *    @param id Id of the element
     *    @param position Position on screen of the element
     *    @param sprites Sprites for the different state of the button
     *    @param text The text to draw on the button
     *    @param charac Characteristic of the text
     *    @param parent The optionnal father of this element
     */
    ButtonElement(Ui* ui,
                  std::string id,
                  sf::Vector2f position,
                  ButtonSprites sprites,
                  std::string text,
                  TextCharacteristic charac,
                  UiElement* parent = nullptr);
    /**
     *    Update the element (and its state)
     *    @param delta Delta time since last update
     *    @param inputState User input state of the current frame
     *    @param lastInputState User input state of the last frame
     */
    void Update(float delta,
                const InputState& inputState,
                const InputState& lastInputState) override;

 protected:
    TextElement textElem;  ///< Text element of this button
    sf::Texture textureActive;  ///< Texture to draw when the button is clicked
    sf::Texture textureHover;  ///< Texture to draw when the mouse is over the button
};

}  // namespace RR

#endif  // SRC_RARIUM_UI_UIELEMENTS_BUTTONELEMENT_H_
