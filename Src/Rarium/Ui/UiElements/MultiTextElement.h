// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RARIUM_UI_UIELEMENTS_MULTITEXTELEMENT_H_

#define SRC_RARIUM_UI_UIELEMENTS_MULTITEXTELEMENT_H_

#include <string>
#include <vector>

#include "Rarium/Ui/UiElements/TextElement.h"

namespace RR {

/**
 *    A text element that can display several predefined text
 */
class MultiTextElement : public TextElement {
 public:
    /**
     *    Create a new multi text element
     *    @param ui The ui possessing this element
     *    @param position Position on screen of the element
     *    @param text The list of text that can be displayed
     *    @param charac Characteristic of the text
     *    @param parent The optionnal father of this element
     */
    MultiTextElement(Ui* ui,
                     std::string id,
                     sf::Vector2f position,
                     std::vector<std::string> text,
                     TextCharacteristic charac,
                     UiElement* parent = nullptr);


    /// Get the list of text that can be display
    inline const std::vector<std::string>& GetTexts() const { return texts; }
    /// Get the text that is currently displayed
    inline unsigned int GetDrawIndex() const { return drawIndex; }

    /// Update the text that is currently displayed
    void UpdateDrawIndex(unsigned int index);

 protected:
    std::vector<std::string> texts;  ///< List of text that can be display
    unsigned int drawIndex;  ///< Index of the text currently displayed
};

}  // namespace RR

#endif  // SRC_RARIUM_UI_UIELEMENTS_MULTITEXTELEMENT_H_
