// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RARIUM_UI_UIELEMENTS_IMAGEELEMENT_H_

#define SRC_RARIUM_UI_UIELEMENTS_IMAGEELEMENT_H_

#include <string>

#include <SFML/Graphics.hpp>

#include "Rarium/Ui/UiElement.h"

namespace RR {

/**
*    An image displayed on screen
*/
class ImageElement : public UiElement {
 public:
    /**
     *    Create a new image element
     *    @param ui The ui possessing this element
     *    @param id Path of the texture to use
     *    @param position Position of the element on screen
     *    @param parent Optionnal parent of the element
     */
    ImageElement(Ui* ui, std::string id,
                 sf::Vector2f position, UiElement* parent = nullptr);

    /// Get the size of the ui element
    inline sf::Vector2f GetSize() override {
        return sf::Vector2f(static_cast<float>(texture.getSize().x),
                            static_cast<float>(texture.getSize().y));
    }


 protected:
    /// Called when this element has been moved
    void ElementMoved(sf::Vector2f offset) override;

    sf::Texture texture;  ///< The image to draw on screen
    sf::Sprite sprite;  ///< The sfml sprite to draw the image on screen
};

}  // namespace RR

#endif  // SRC_RARIUM_UI_UIELEMENTS_IMAGEELEMENT_H_
