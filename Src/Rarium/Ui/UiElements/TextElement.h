// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RARIUM_UI_UIELEMENTS_TEXTELEMENT_H_

#define SRC_RARIUM_UI_UIELEMENTS_TEXTELEMENT_H_

#include <string>

#include "Rarium/Ui/UiElement.h"

namespace RR {

/// Characteristic of a text
struct TextCharacteristic {
    std::string Font;  ///< Path to the font used to draw text
    unsigned int FontSize;  ///< Size of the text to draw
    sf::Color Color = sf::Color::Black;  ///< Color of the text to draw
};

/**
 *    A text displayed on screen
 */
class TextElement : public UiElement {
 public:
    /**
    *    Create a new text element
    *    @param ui The ui possessing this element
    *    @param id Id of the element
    *    @param position Position on screen of the element
    *    @param text The text to draw
    *    @param charac Characteristic of the text
    *    @param parent The optionnal father of this element
    */
    TextElement(Ui* ui,
                std::string id,
                sf::Vector2f position,
                std::string text,
                TextCharacteristic charac,
                UiElement* parent = nullptr);

    /// Update the text displayed by this element
    void UpdateText(std::string text);

    /// Get the text display by this element
    inline std::string GetText() const { return text.getString(); }

    /// Get the underlying SFML text
    inline const sf::Text& GetSFText() const { return text; }

    /// Get the size of the ui element
    inline sf::Vector2f GetSize() override {
        return sf::Vector2f(text.getLocalBounds().width,
                            text.getLocalBounds().height);
    }

 protected:
    /// Called when this element has been moved
    void ElementMoved(sf::Vector2f offset) override;

    sf::Text text;  ///< Text to draw on screen
    sf::Font font;  ///< Font used to draw the text
};

}  // namespace RR

#endif  // SRC_RARIUM_UI_UIELEMENTS_TEXTELEMENT_H_
