// Copyright RariumUnlimited - Licence : MIT
#include "Rarium/Ui/UiElements/ImageElement.h"

namespace RR {

ImageElement::ImageElement(Ui* ui, std::string id,
                           sf::Vector2f position, UiElement* parent) :
    UiElement(ui, id, position, parent) {
    texture.loadFromFile(id);
    sprite.setTexture(texture);
    sprite.setPosition(this->GetPosition().x, this->GetPosition().y);
    drawables.push_back(&sprite);
}

void ImageElement::ElementMoved(sf::Vector2f offset) {
    sf::Vector2f pos = sprite.getPosition();
    sprite.setPosition(pos.x + offset.x, pos.y + offset.y);
}

}  // namespace RR
