// Copyright RariumUnlimited - Licence : MIT
#include "Rarium/Ui/UiElements/MultiTextElement.h"

#include <cassert>

#include "Rarium/Tools/Math.h"

namespace RR {

MultiTextElement::MultiTextElement(Ui* ui,
                                   std::string id,
                                   sf::Vector2f position,
                                   std::vector<std::string> text,
                                   TextCharacteristic charac,
                                   UiElement* parent) :
    TextElement(ui, id, position, "", charac, parent),
    texts(text) {
    assert(text.size() > 0);
    UpdateDrawIndex(0);
}


void MultiTextElement::UpdateDrawIndex(unsigned int index) {
    drawIndex = index;
    drawIndex = Clamp<unsigned int>(drawIndex,
                                    0,
                                    static_cast<unsigned int>(texts.size()) - 1);
    this->text.setString(texts[drawIndex]);
}

}  // namespace RR
