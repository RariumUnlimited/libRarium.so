// Copyright RariumUnlimited - Licence : MIT
#include "Rarium/Ui/UiElement.h"

#include <iostream>

namespace RR {

bool UiElement::mouseIntercepted = false;

UiElement::UiElement(Ui* ui,
                     std::string id,
                     sf::Vector2f position,
                     UiElement* parent) :
    ui(ui),
    enabled(true),
    position(position),
    parent(parent),
    id(id) {
    if (parent != nullptr) {
        this->position += parent->position;
        parent->Move += new MoveEvent::T<UiElement>(this, &UiElement::MoveFromOffset);
    }

    this->Move += new MoveEvent::T<UiElement>(this, &UiElement::ElementMoved);
}

void UiElement::GetFullChildren(std::vector<UiElement*>& list) const {
    list.insert(list.end(), children.begin(), children.end());
    for (UiElement* e : children) {
        e->GetFullChildren(list);
    }
}

void UiElement::MoveToPosition(sf::Vector2f position) {
    if (parent != nullptr)
        position += parent->position;

    sf::Vector2f offset = position - this->position;
    this->position = position;
    Move(offset);
}

void UiElement::MoveFromOffset(sf::Vector2f offset) {
    this->position += offset;
    Move(offset);
}

void UiElement::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    for (sf::Drawable* d : drawables)
        target.draw(*d, states);

    for (UiElement* e : children)
        target.draw(*e, states);
}

void UiElement::Update(float delta,
                       const InputState& inputState,
                       const InputState& lastInputState) {
    if (enabled) {
        for (int i = static_cast<int>(children.size()) - 1; i >= 0; --i)
            children[i]->Update(delta, inputState, lastInputState);

        state.WasHover = state.IsHover;
        state.WasFocus = state.IsFocus;
        state.WasClicked = state.IsClicked;
        state.WasActive = state.IsActive;

        MouseEventArg eventArgs(inputState.MousePosition);

        // If mouse is inside the UiElement, it grabs the mouse and elements below cannot use the mouse.
        if (inputState.MousePosition.x > position.x &&
            inputState.MousePosition.x < position.x + GetSize().x &&
            inputState.MousePosition.y > position.y &&
            inputState.MousePosition.y < position.y + GetSize().y &&
            !mouseIntercepted) {
            mouseIntercepted = true;
            state.IsHover = true;

            if (!state.WasHover)
                Hover(eventArgs);

            if (inputState.ButtonPressed.find(sf::Mouse::Button::Left) != inputState.ButtonPressed.cend()) {
                state.IsActive = true;
                if (lastInputState.ButtonPressed.find(sf::Mouse::Button::Left) == inputState.ButtonPressed.cend()) {
                    state.IsFocus = true;
                    state.IsClicked = true;
                    if (!state.WasFocus)
                        Focus(eventArgs);
                }
            } else {
                state.IsActive = false;
            }
            if (lastInputState.ButtonPressed.find(sf::Mouse::Button::Left) != inputState.ButtonPressed.cend() &&
                inputState.ButtonPressed.find(sf::Mouse::Button::Left) == inputState.ButtonPressed.cend() &&
                state.IsClicked) {
                state.IsClicked = false;
                state.IsActive = false;
                Click(eventArgs);
            }
        } else {
            if (state.WasHover) {
                state.IsHover = false;
                Out(eventArgs);
            }

            state.IsActive = false;
            if (inputState.ButtonPressed.find(sf::Mouse::Button::Left) != inputState.ButtonPressed.cend() &&
                lastInputState.ButtonPressed.find(sf::Mouse::Button::Left) == inputState.ButtonPressed.cend()) {
                state.IsFocus = false;
                if (state.WasFocus)
                    Blur(eventArgs);
            }
            if (inputState.ButtonPressed.find(sf::Mouse::Button::Left) == inputState.ButtonPressed.cend()) {
                state.IsClicked = false;
            }
        }
    }
}

void UiElement::ResetMouseInterception() {
    mouseIntercepted = false;
}

}  // namespace RR
