// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RARIUM_UI_MOUSEEVENTARG_H_

#define SRC_RARIUM_UI_MOUSEEVENTARG_H_

#include <SFML/System.hpp>

namespace RR {

/**
 *	Arguments given when a mouse event is triggered
 */
class MouseEventArg {
 public:
    /**
     *	Create a new MouseEventArg
     *	@param mousePosition Position of mouse on screen at the time event
     */
    explicit MouseEventArg(sf::Vector2i mousePosition) : mousePosition(mousePosition) { }

    /// Get the position of mouse on screen a time of event
    inline const sf::Vector2i& GetMousePosition() { return mousePosition; }

 private:
    sf::Vector2i mousePosition;  ///< Position of mouse on screen at the time of event
};

}  // namespace RR

#endif  // SRC_RARIUM_UI_MOUSEEVENTARG_H_
