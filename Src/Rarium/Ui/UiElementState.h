// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RARIUM_UI_UIELEMENTSTATE_H_

#define SRC_RARIUM_UI_UIELEMENTSTATE_H_

namespace RR {

/**
 *	State of a ui element
 */
struct UiElementState {
    bool WasClicked = false;  ///< If the element was clicked last frame
    bool IsClicked = false;  ///< If the element is clicked in the current frame
    bool WasHover = false;  ///< If the mouse was hover the element in the last frame
    bool IsHover = false;  ///< If the mouse is hover the element in the current frame
    bool WasFocus = false;  ///< If the element was focused last frame
    bool IsFocus = false;  ///< If the element is focused in the current frame
    bool WasActive = false;  ///< If the element was active in the last frame (was being clicked on)
    bool IsActive = false;  ///< If the element is active in the current frame (is being clicked on)
};

}  // namespace RR

#endif  // SRC_RARIUM_UI_UIELEMENTSTATE_H_
