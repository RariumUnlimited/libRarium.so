// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RARIUM_UI_UI_H_

#define SRC_RARIUM_UI_UI_H_

#include <vector>

#include <SFML/Graphics.hpp>

#include "Rarium/Ui/UiElement.h"

namespace RR {

/**
 *    A User Interface to display on screen
 */
class Ui : public sf::Drawable {
 public:
    /**
     *    Destroy the ui
     */
    virtual ~Ui() { }

    /// Get the list of all ui element that are in the ui
    inline const std::vector<UiElement*>& GetElements() const {
        return elements;
    }

    /**
     *    Add a new ui element to the ui
     *    return Pointer to the new element added
     */
    UiElement* AddElement(UiElement* pElem);

    /**
     *    Update the ui, when override this method, don't forget to call for Ui::Update
     *    @param delta Delta time since last update
     *    @param state State of user input in current frame
     *    @param lastState State of user input in last frame
     */
    virtual void Update(float delta,
                        const InputState& state,
                        const InputState& lastState);

    /**
     * @brief Draw the ui element on screen
     * @param target Render target to draw to
     * @param states Current render states
     */
    void draw(sf::RenderTarget &target, sf::RenderStates states) const;

 private:
    std::vector<UiElement*> elements;  ///< List of all ui element that are in the ui
};

}  // namespace RR

#endif  // SRC_RARIUM_UI_UI_H_
