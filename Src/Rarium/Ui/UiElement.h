// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RARIUM_UI_UIELEMENT_H_

#define SRC_RARIUM_UI_UIELEMENT_H_

#include <string>
#include <vector>

#include <SFML/Graphics.hpp>

#include "Rarium/Tools/Event.h"
#include "Rarium/Ui/UiElementState.h"
#include "Rarium/Ui/MouseEventArg.h"
#include "Rarium/Window/InputState.h"

namespace RR {

class Ui;

typedef Event<void, MouseEventArg> MouseEvent;
typedef Event<void, sf::Vector2f> MoveEvent;

/**
 *    A ui element (commonly known as Widget), that can be drawn on screen as part of an ui
 */
class UiElement : public sf::Drawable {
 public:
    /**
     *    Create a new UiElement
     *    @param ui The ui that posses the element
     *    @param position Position of the element on screen relative to the parent
     *    @param parent An optionnal element that can be a parent of this element
     *    @param isDynamic If the element contains resources that can be dynamic
     */
    UiElement(Ui* ui,
              std::string id,
              sf::Vector2f position = sf::Vector2f(0.f, 0.f),
              UiElement* parent = nullptr);
    /**
    *    Destroy the ui element
    */
    virtual ~UiElement() { }

    /// Get the id of the element
    inline const std::string& GetId() const { return id; }
    /// Get the list of resources used by the element
    /// Get the list of element children that this element have
    inline std::vector<UiElement*> GetChildren() { return children; }
    /// Get the state of the element
    inline const UiElementState& GetState() const { return state; }
    /// Get if the element is enabled/updatable
    inline bool IsEnabled() const { return enabled; }
    /// Set if the element is enabled/updatable
    inline void SetEnabled(bool enabled) { this->enabled = enabled; }
    /// Get the position of the element
    inline const sf::Vector2f& GetPosition() const { return position; }
    /// Get the size of the ui element
    virtual sf::Vector2f GetSize() = 0;

    /// Move the element to a new position
    void MoveToPosition(sf::Vector2f position);
    /// Move the element by an offset
    void MoveFromOffset(sf::Vector2f offset);

    /// Get the full lsit of children and their children and ... of this element
    void GetFullChildren(std::vector<UiElement*>& list) const;

    /**
    *   Update the element (and its state), subclasses that override this method should call UiElement::Update
    *   @param delta Delta time since last update
    *   @param inputState User input state of the current frame
    *   @param lastInputState User input state of the last frame
    */
    virtual void Update(float delta,
                        const InputState& inputState,
                        const InputState& lastInputState);

    MouseEvent Hover;  ///< Event that is triggered when the element is hover by the mouse
    MouseEvent Out;  ///< Event that is triggered when the mouse leave the element
    MouseEvent Focus;  ///< Event that is triggered when the element is focused
    MouseEvent Blur;  ///< Event that is triggered when the element loses focus
    MouseEvent Click;  ///< Event that is triggered when the element is clicked

    MoveEvent Move;  ///< Event that is triggered when the element is moved

    static void ResetMouseInterception();

    /**
     * @brief Draw the ui element on screen
     * @param target Render target to draw to
     * @param states Current render states
     */
    void draw(sf::RenderTarget &target, sf::RenderStates states) const;

 protected:
    /// Called when this element has been moved
    virtual void ElementMoved(sf::Vector2f offset) = 0;

    std::vector<UiElement*> children;  ///< List of children of the element, needs to be filled by subclasses
    std::vector<sf::Drawable*> drawables;  ///< Drawable of the element, needs to be filled by subclasses

 private:
    Ui* ui;  ///< The ui that posses the element
    UiElementState state;  ///< Current state of the element
    bool enabled;  ///< If the element can be interacted with
    sf::Vector2f position;  ///< Position of the element
    UiElement* parent;  ///< An optionnal parent of this element
    std::string id;  ///< Id of the element

    static bool mouseIntercepted;  ///< If the mouse has been use by one UiElement during current update.
};

}  // namespace RR

#endif  // SRC_RARIUM_UI_UIELEMENT_H_
